#!/bin/bash

#
# This script is created by : Ismail Dhaoui
#

function show_usage (){
    printf "Usage: $0 [option] LAB-GUID USERNAME PASSWORD NB_STUDENTS \n"
    printf " LAB-GUID: 4 characters string that define the lab\n"
    printf " USERNAME: Ansible Tower user with admin priviledges, usually admin\n"
    printf " PASSWORD: user's password\n"
    printf " NB_STUDENTS: Number of student's lab available to be reset \n\n"
    printf "Options:\n"
    printf " -h|--help, Print help\n"

return 0
}

function progress {
spin='-\|/'
i=0
while kill -0 $MYPID 2>/dev/null
do
  i=$(( (i+1) %4 ))
  printf "\r${spin:$i:1}"
  sleep .1
done
}

function tower_reset() {
for STUDENT in `seq 1 $STUDENTS`; do
    printf "Deleting all resources for Student $STUDENT \n"
    for RESOURCE in "${RESOURCES[@]}";do
        awx --conf.host $PREF$STUDENT$SUFF --conf.username $USERNAME --conf.password $PASSWORD -k $RESOURCE list -f human | awk  '{ print $1 }' | awk '/==/{y=1;next}y' | while read -r ITEM; do   
        awx --conf.host $PREF$STUDENT$SUFF --conf.username $USERNAME --conf.password $PASSWORD -k $RESOURCE delete $ITEM
        done
    done &>/dev/null &
    MYPID=$!
    progress
done 
return 0
}

PREF='https://student'
SUFF='.'$1'.open.redhat.com'
USERNAME="$2"
PASSWORD="$3"
STUDENTS="$4"
RESOURCES=('workflow_job_templates' 'job_templates' 'projects' 'inventory' 'credentials' 'unified_jobs')
if ! command -v awx &> /dev/null
then
    echo "awx could not be found, Try installing: ansible-tower-cli "
    exit
fi
if [ $# -eq 4 ]; then
    tower_reset;
  else
    show_usage;
fi
