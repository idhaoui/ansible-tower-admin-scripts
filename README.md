# ansible-tower-admin-scripts

# tower-reset.sh
 This script is created to help resetting Red Hat Ansible Tower to factory settings. It removes all **jobs, workflow_job_templates, job_templates, projects inventory and credentials**.

It iterates over a number of students. This script expects 4 parametes: **LAB-GUID USERNAME PASSWORD NB_STUDENTS**:

- **LAB-GUID**: 4 characters string that identifies the lab.<br>
- **USERNAME**: Ansible Tower user with admin priviledges, usually admin.<br>
- **PASSWORD**: user's password.<br>
- **NB_STUDENTS**: Number of student's lab available to be reset<br>

Script will iterate in iterate on all hosts that have the URL pattern like : **`'https://student'<$number>'.open.redhat.com'`**
